Commerce Recommender
====================

This module makes 2 types of Commerce Product recommendations:

  * "Users who purchased this product also purchased"
  * "You might want to purchase other products based on your purchase history"


Limitations
-----------

  * This module works only with Commerce Product, Commerce Order and Commerce Line Item. It might not work if you customize any of those modules.
  * The default Views link products to their "administrative view". You might need to customize the link for your own "Product Display" content type.
  * This module is supposed to work as an example. You might need to customize the module and build your own commerce recommender for your particular needs.


Installation & Configuration
----------------------------

You need to install the Recommender API (7.x-6.x) module and Drupal Computing module before install this module.

After installation, follow these steps to compute recommendations:

  1. Run Drupal Cron to feed product purchase history into recommender.
  2. Go to admin/config/system/computing/list, and click "Commerce Recommender" to add a computing command.
  3. Compute recommendations using either of the following approaches:
    - Open a command line terminal and run "drush recommender-run".
    - Open a command line terminal and execute the Recommender Java agent.
    - Go to admin/config/system/computing/recommender and click "Run Recommender" (not recommended).
  . You can view the execution results at admin/config/system/computing/records.
  4. Display recommendation results using the default Views.

For more information about how the module works, please read the documentation of Recommender API.

