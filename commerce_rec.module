<?php


/**
 * Implements of hook_help().
 */
function commerce_rec_help($path, $args) {
  $output = '';
  switch ($path) {
    case "admin/help#commerce_rec":
      $output = '<p>'.  t("This module generates Commerce products recommendations based on users purchasing history") .'</p>';
      break;
  }
  return $output;
}


/**
 * Implements hook_views_api().
 */
function commerce_rec_views_api() {
  return array(
    'api' => 3,
  );
}


/**
 * Implements hook_recommender_data().
 */
function commerce_rec_recommender_data() {
  return array(
    'commerce_rec_default' => array(
      'title' => t('Commerce Recommender'),
      'description' => t('Generate product recommendations based on users purchasing history. See technical details in README.txt.'),
      'algorithm' => 'item2item_boolean',
      'data structure' => array(
        'preference' => array(
          'type' => 'table',
          'name' => 'commerce_rec_purchase',
          'score type' => 'boolean',
          'no views' => TRUE, // use Commerce default views instead for purchasing history.
        ),
        'item similarity' => array(
          'type' => 'table',
          'name' => 'commerce_rec_similarity',
        ),
        'prediction' => array(
          'type' => 'table',
          'name' => 'commerce_rec_prediction',
        ),
        'item entity type' => 'commerce_product',
      ),
      'cron callback' => 'commerce_rec_cron_callback',
      'upkeep callback' => 'commerce_rec_upkeep_callback'
    ),
  );
}


function commerce_rec_cron_callback($curr) {
  $prev = variable_get('commerce_rec_last_cron', 0);

  $rows = db_query("SELECT o.uid, p.product_id, MAX(li.changed) AS updated FROM {commerce_line_item} li INNER JOIN {commerce_order} o ON li.order_id = o.order_id INNER JOIN {commerce_product} p ON li.line_item_label = p.sku WHERE o.uid <> 0 AND o.status = 'completed' AND p.status = 1 AND li.changed BETWEEN :prev AND :curr GROUP BY o.uid, p.product_id", array(':prev' => $prev, ':curr' => $curr))->fetchAll();

  foreach ($rows as $row) {
    db_merge('commerce_rec_purchase')->key(array('uid' => $row->uid, 'eid' => $row->product_id))
      ->insertFields(array('uid' => $row->uid, 'eid' => $row->product_id, 'updated' => $row->updated))
      ->updateFields(array('updated' => $row->updated))
      ->execute();
  }

  variable_set('commerce_rec_last_cron', $curr);
}


function commerce_rec_upkeep_callback($timestamp) {
  // this is supposed to remove canceled order data, etc. not implemented yet.
}
