<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_rec_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'commerce_recommender_users_who_purchased_this_also_purchased';
  $view->description = 'Display "users who purchased this also purchased on product page".';
  $view->tag = 'recommender';
  $view->base_table = 'commerce_rec_similarity';
  $view->human_name = 'Commerce Recommender: Users Who Purchased This Also Purchased';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Commerce Recommender: Users Who Purchased This Also Purchased';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Join the commerce_product table (on item 2) */
  $handler->display->display_options['relationships']['eid2']['id'] = 'eid2';
  $handler->display->display_options['relationships']['eid2']['table'] = 'commerce_rec_similarity';
  $handler->display->display_options['relationships']['eid2']['field'] = 'eid2';
  $handler->display->display_options['relationships']['eid2']['ui_name'] = 'Join the commerce_product table (on item 2)';
  $handler->display->display_options['relationships']['eid2']['required'] = TRUE;
  /* Field: Commerce Product: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'eid2';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_product'] = 1;
  /* Sort criterion: Recommender: Commerce Recommender: score in item similarity table */
  $handler->display->display_options['sorts']['score']['id'] = 'score';
  $handler->display->display_options['sorts']['score']['table'] = 'commerce_rec_similarity';
  $handler->display->display_options['sorts']['score']['field'] = 'score';
  $handler->display->display_options['sorts']['score']['order'] = 'DESC';
  /* Contextual filter: Filter by product_id of the commerce_product entity (on item 1) */
  $handler->display->display_options['arguments']['eid1']['id'] = 'eid1';
  $handler->display->display_options['arguments']['eid1']['table'] = 'commerce_rec_similarity';
  $handler->display->display_options['arguments']['eid1']['field'] = 'eid1';
  $handler->display->display_options['arguments']['eid1']['ui_name'] = 'Filter by product_id of the commerce_product entity (on item 1)';
  $handler->display->display_options['arguments']['eid1']['default_action'] = 'default';
  $handler->display->display_options['arguments']['eid1']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['eid1']['default_argument_options']['index'] = '3';
  $handler->display->display_options['arguments']['eid1']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['eid1']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['eid1']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Commerce Product: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'eid2';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  /* Filter criterion: Recommender: Commerce Recommender: score in item similarity table */
  $handler->display->display_options['filters']['score']['id'] = 'score';
  $handler->display->display_options['filters']['score']['table'] = 'commerce_rec_similarity';
  $handler->display->display_options['filters']['score']['field'] = 'score';
  $handler->display->display_options['filters']['score']['operator'] = '>=';
  $handler->display->display_options['filters']['score']['value']['value'] = '0.1';
  $handler->display->display_options['filters']['score']['group'] = 1;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $views[$view->name] = $view;


  $view = new view();
  $view->name = 'commerce_recommender_my_recommendations';
  $view->description = 'Personalized recommendations based on users purchase history';
  $view->tag = 'recommender';
  $view->base_table = 'commerce_rec_prediction';
  $view->human_name = 'Commerce Recommender: My Recommendations';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Commerce Recommender: My Recommendations';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Join the commerce_product table */
  $handler->display->display_options['relationships']['eid']['id'] = 'eid';
  $handler->display->display_options['relationships']['eid']['table'] = 'commerce_rec_prediction';
  $handler->display->display_options['relationships']['eid']['field'] = 'eid';
  $handler->display->display_options['relationships']['eid']['ui_name'] = 'Join the commerce_product table';
  $handler->display->display_options['relationships']['eid']['required'] = TRUE;
  /* Field: Commerce Product: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'eid';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_product'] = 1;
  /* Sort criterion: Recommender: Commerce Recommender: score in prediction table */
  $handler->display->display_options['sorts']['score']['id'] = 'score';
  $handler->display->display_options['sorts']['score']['table'] = 'commerce_rec_prediction';
  $handler->display->display_options['sorts']['score']['field'] = 'score';
  $handler->display->display_options['sorts']['score']['order'] = 'DESC';
  /* Contextual filter: Filter by current user */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'commerce_rec_prediction';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['ui_name'] = 'Filter by current user';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Commerce Product: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'eid';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Recommender: Commerce Recommender: score in prediction table */
  $handler->display->display_options['filters']['score']['id'] = 'score';
  $handler->display->display_options['filters']['score']['table'] = 'commerce_rec_prediction';
  $handler->display->display_options['filters']['score']['field'] = 'score';
  $handler->display->display_options['filters']['score']['operator'] = '>=';
  $handler->display->display_options['filters']['score']['value']['value'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $views[$view->name] = $view;

  return $views;
}